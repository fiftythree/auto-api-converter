# Auto API converter

Uses AI to speed up the process of data conversion.  Just add your OpenAI API key, and your off to the races!

## High level

Intended as a heuristic for converting either similar data coming in from external sources to your format, or converting your data for sending externally.  I run into this scenario a lot exploring different data sources, so it is meant for solving 95% of the problem, and then finishing the data connection with more careful review after.

Here's an example for getting from external source:

```mermaid
graph LR
  subgraph your environment
  d[(internal data)]
  r(response json)
  f[downstream function]
  end
  ep[external API with similar data]
  ep --> r
  d --> f
  r -.->|How to integrate?| f
```

Take a sample of the internal data, some sample response jsons, put that into the converter. Out comes a `convert_format` function that you can use.

```mermaid
graph LR
  d[(internal data)]
  ds[internal data sample]
  r(response json)
  f[downstream function]
  o[convert_format]
  a[auto api converter]
  d -.-> ds -.-> a
  r -.-> a
  a -.-> o
  r --> o --> f
```

This `convert_format` can then be used to convert the endpoint's response to your format (or your format into the target's format)


## Installation

To install the `auto-ai-converter` client library...

1. use pip:

```bash
pip install auto-api-converter python-dotenv
```

2. Then, add your Open AI API key to the .env file in your project's root directory.

```
OPENAI_API_KEY=<put your API key here>
```

## The prompt

See [AutoConverter.prompt_text](https://gitlab.com/fiftythree/auto-api-converter/-/blob/main/auto_api_converter/auto_api_converter.py)

## Usage

After installation, you can use the converter. Below is a basic usage example:


```python
import pandas as pd
import json

# 1. read your data
df = pd.read_csv("event-input-file-pre-model.csv")
df.head() # the dataframe before model pre-processing

"""
returns...
ObsId | Bookmaker | homeTeam | awayTeam | leagueName | gametime | spreadAway | spreadHome
123   | Fanduel   | Pittsburgh | Baltimore | mlb | May | 1.5 | -1

"""

# 2. read sample external responses
## make the requests how you plan to use in pipeline..
import requests
headers = {
    #"Authorization": f"Bearer {self.api_key}",
    "Content-Type": "application/json"
    }
API_ENDPOINT = "https://fiftythree.app/example-data"
response = requests.get(API_ENDPOINT, headers=headers).json()

## if stored locally...
# with open("example-api-response1.json", 'r') as fn:
#     response = json.load(fn)

# 3. pass these to the converter function
from auto_api_converter import AutoConverter

# by default, the converter will not take more than 5 rows of sample data

converter_params = {
    # openai_api_key: # PUT KEY HERE (if not using a .env)
    source_name: "fiftythree_api", # use a-z characters or underscores
    target_name: "internal",       # use a-z characters or underscores
    source_data: response,         # pd.DataFrame or dict expected
    target_data: df,               # pd.DataFrame or dict expected
}

if __name__ == "__main__":
    # load_dotenv() # uncomment (if using a .env)
    converter = AutoConverter(**converter_params)
    print(converter.convert())
```

Running this script will output the code to your command prompt.  You can then copy this code to your repo, name it accordingly.  It may not be perfect, but it's a starting point to write your code.

## Contributing

If you wish to contribute to this library, please fork the repository, make your changes, and submit a merge request. We value your input!

## Test

See the `main.py`

## License

MIT License

## Support

For any questions or issues, please open an issue [here](https://gitlab.com/fiftythree/auto-api-converter/-/issues) or reach out to <evan@fiftythree.app>
