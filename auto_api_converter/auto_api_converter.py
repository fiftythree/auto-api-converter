import os
import requests
import pandas as pd
import json
from typing import Union
from jinja2 import Environment, PackageLoader, select_autoescape

class AutoConverter:
    """Convert data formats and get the converter function with chatGPT.

    Expects source and target examples.  After testing a few times,
    make sure to run with gpt-4, as results will be better.  Also obviously, 
    the more examples the better, but the more the cost will be.  3-4 rows 
    for each should be enough.

    I've added gpt_version as an openAI API parameter, but these can
    be overridden with the custom openai_kwargs.  for example:

      AutoConverter(..., max_tokens=3000)

    means that the `max_tokens` argument in the openAI API will
    be set to 3000.  All variables are stored in self.openai_api_params
    """
    def __init__(self, 
                 source_name: str, 
                 target_name: str, 
                 source_data: Union[pd.DataFrame, dict, list], 
                 target_data: Union[pd.DataFrame, dict, list],
                 openai_api_key:str=None, 
                 gpt_version:str="gpt-3.5-turbo",
                 **openai_kwargs,
                 ):

        self.source_name = source_name
        self.target_name = target_name
        self.source_data = source_data
        self.target_data = target_data
        self.openai_api_key = openai_api_key
        self.openai_api_params = {
            "model": gpt_version,
            "max_tokens": 1500,
            "top_p": 1,
            "temperature": 0.5,
            "frequency_penalty": 0,
            "presence_penalty": 0,
        }

        self.prep_examples_for_prompt()
        self.prompt_template = self.get_template('api_convert.jinja')
        self.prompt_text = self.prompt_template.render(
            source_examples=self.source_examples,
            target_examples=self.target_examples,
        )

        # override arguments for the openAI API
        for k, v in openai_kwargs:
            self.openai_api_params[k] = v

        if not self.openai_api_key:
            self.openai_api_key = os.getenv('OPENAI_API_KEY')

        if not self.openai_api_key:
            raise ValueError("OPENAI_API_KEY not set -- add to environment "
                             "variables or pass in manually.")

    def convert(self):
        print(f"Converting data from {self.source_name} to {self.target_name}")
        ai_output = self.send_to_chatgpt(self.prepare_payload())
        print(ai_output)  # You can customize this output

    def get_template(self, template_name):
        env = Environment(
            loader=PackageLoader('auto_api_converter', 'templates'),
            #autoescape=select_autoescape(['txt'])
        )
        template = env.get_template(template_name)
        return template

    def prep_examples_for_prompt(self):
        self.source_examples = self.prep_example_data(self.source_data)
        self.target_examples = self.prep_example_data(self.target_data)

    def prep_example_data(self, data):
        if isinstance(data, pd.DataFrame):
            sample_data = data.T.to_dict()

        elif isinstance(data, list):
            sample_data = {k: row for k, row in enumerate(data, start=0)}

        elif isinstance(data, dict):
            sample_data = data

        else:
            raise Exception("data in wrong format")

        return sample_data


    def prepare_payload(self, out_string=True):
        # Convert DataFrame to JSON strings if needed
        source_examples = self.generate_example_text(source=True)
        target_examples = self.generate_example_text(source=False)

        # Prepare the natural language prompt
        prompt = self.prompt_text.format(
                    source_examples_str="\n".join(source_examples),
                    target_examples_str="\n".join(target_examples),
                    )

        payload = {
            "messages": [
                {"role": "user",
                 "content": prompt,
                }
            ],
            **self.openai_api_params
            }

        return json.dumps(payload) if out_string else payload

    def send_to_chatgpt(self, payload):

        headers = {
            "Authorization": f"Bearer {self.openai_api_key}",
            "Content-Type": "application/json"
        }

        response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, data=payload)
        if response.status_code == 200:
            response_body = response.json()
            if "choices" in response_body:
                return response_body["choices"][0]['message']["content"]
            else:
                return response.json()  # Return the response JSON for further processing
        else:
            raise Exception(f"Failed to send data to OpenAI API: {response.status_code} - {response.text}")


# Example usage
if __name__ == "__main__":
    from .examples import sample_target1, json_source1

    df_target = pd.DataFrame(
                    data=[sample_target1[1]],
                    columns=sample_target1[0])
    

    # Example usage showing environment variable requirement
    converter = AutoConverter(
                    source_name='fiftythree_app',
                    target_name="internal",
                    source_data=json_source1,
                    target_data=df_target,
                    )
    
    print(converter.prepare_payload(out_string=False))

    #converter.convert()
