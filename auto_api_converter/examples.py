"""
"""
import pandas as pd

df_target1 = pd.DataFrame(
            columns=["GameId", "Bookmaker", "homeTeam", "awayTeam", 
                     "leagueName", "gametime", "spreadAway", "spreadHome"],
            data=[
                [1233, "Draftkings", "COL", "MIA", "mlb", "May 2, 2024, 12:10 PM", 1.5, -1],
                [1234, "Draftkings", "NYY", "BAL", "mlb", "May 2, 2024, 1:10 PM", -1, 1.5]
                ])

json_source1 = [
        {
        "completed": True,
        "scoreL": 4,
        "lastUpdateTime": "2024-05-02T16:00:58.714Z",
        "observation": {
        "totalUOdds": -110,
        "spreadROdds": 124,
        "totalU": 7.5,
        "spreadL": 1.5,
        "lWinOdds": 145,
        "rWinOdds": -175,
        "totalOOdds": -110,
        "spreadR": -1,
        "spreadLOdds": -148,
        "totalO": 7.5
        },
        "bookmakerName": "draftkings",
        "ttl": 1714752034,
        "playable": {
        "eventTime": "2024-05-02T16:10:00.000Z",
        "competitorR": "Miami Marlins",
        "competitorL": "Colorado Rockies",
        "eventDateKey": 20240502
        },
        "scoreR": 5,
        "playableId": 4534,
        "league": "mlb"
        },
        {
        "completed": True,
        "scoreL": 2,
        "lastUpdateTime": "2024-05-02T17:01:28.852Z",
        "observation": {
        "totalUOdds": -118,
        "spreadROdds": -180,
        "totalU": 9,
        "spreadL": -1,
        "lWinOdds": -105,
        "rWinOdds": -115,
        "totalOOdds": -102,
        "spreadR": 1.5,
        "spreadLOdds": 150,
        "totalO": 9
        },
        "bookmakerName": "draftkings",
        "ttl": 1714751433,
        "playable": {
        "eventTime": "2024-05-02T17:05:00.000Z",
        "competitorR": "Baltimore Orioles",
        "competitorL": "New York Yankees",
        "eventDateKey": 20240502
        },
        "scoreR": 7,
        "playableId": 4542,
        "league": "mlb"
        },
    ]