from setuptools import setup, find_packages

with open("requirements.txt", 'r') as fn:
    requirements = [line.strip() for line in fn]

setup(
    name='auto-api-converter',
    version='0.1.0',  # Start with a small version number and iterate from there
    packages=find_packages(),  # Automatically discover and include all packages in the package directory
    install_requires=requirements,
    author='Evan Jenkins',
    author_email='evan@fiftythree.app',
    description='Python client for interacting with the FiftyThree API.',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    include_package_data=True,
    url='https://gitlab.com/fiftythree/auto-api-converter',
    classifiers=[
        # Choose the right classifiers as you see fit from: https://pypi.org/classifiers/
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    license='MIT',
    python_requires='>=3.6',  # Specify a minimum version
)