"""Testing the function output.
1. run the code with your data (example shown below) 
2. copy the function into this script
3. test using the data you gave to train
"""
import pandas as pd
import datetime
from auto_api_converter import df_target1, json_source1
from auto_api_converter import AutoConverter

# 1.
# Run the converter
def get_convert_code():
    converter = AutoConverter(
       source_name="fiftythree_app",
       target_name="target_name",
       source_data=json_source1,
       target_data=df_target1,
       # use gpt-4 when tested properly.
       gpt_version="gpt-3.5-turbo", #"gpt-4-turbo",
       )
    converter.convert()

## uncomment here to run the converter
#get_convert_code()

## Paste the output function below

# 2.
# Below is the AI generated function
######                               ######
######  PASTE RESULT CODE HERE START ######
######                               ######
def convert_format1(source):
    team_abbreviations = {
        "Miami Marlins": "MIA",
        "Colorado Rockies": "COL",
        "Baltimore Orioles": "BAL",
        "New York Yankees": "NYY"
    }
    event_time = datetime.datetime.strptime(source['playable']['eventTime'], "%Y-%m-%dT%H:%M:%S.%fZ")
    print(event_time)
    formatted_time = event_time.strftime(r"%B %d, %Y, %I:%M %p")
    home_team = team_abbreviations[source['playable']['competitorL']]
    away_team = team_abbreviations[source['playable']['competitorR']]
    spread_home = source['observation']['spreadR']
    spread_away = source['observation']['spreadL']
    target = {
        'GameId': source['playableId'],
        'Bookmaker': source['bookmakerName'].capitalize(),
        'homeTeam': home_team,
        'awayTeam': away_team,
        'leagueName': source['league'],
        'gametime': formatted_time,
        'spreadAway': spread_away,
        'spreadHome': spread_home
    }
    return target

def convert_format2(source):
    def convert_time_to_local_format(event_time):
        utc_time = datetime.datetime.strptime(event_time, "%Y-%m-%dT%H:%M:%S.%fZ")
        local_time = utc_time - datetime.timedelta(hours=4)  # Convert UTC to Eastern Time (ET)   
        return local_time.strftime("%b %d, %Y, %I:%M %p")

    def convert_api_data_to_internal_format(source_data):
        internal_format = {
            'GameId': source_data['playableId'],
            'Bookmaker': source_data['bookmakerName'].capitalize(),
            'homeTeam': source_data['playable']['competitorL'][:3].upper(),
            'awayTeam': source_data['playable']['competitorR'][:3].upper(),
            'leagueName': source_data['league'],
            'gametime': convert_time_to_local_format(source_data['playable']['eventTime']),       
            'spreadAway': source_data['observation']['spreadR'],
            'spreadHome': source_data['observation']['spreadL']
        }
        return internal_format
    
    return convert_api_data_to_internal_format(source)

######                             ######
######  PASTE RESULT CODE HERE END ######
######                             ######

## 3.
# Test on the same data
print(df_target1.T)
#
print(pd.DataFrame([convert_format2(x) for x in json_source1]).T)
